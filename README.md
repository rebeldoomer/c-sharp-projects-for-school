# C-Sharp Projects for School

Not a fan of C#, did get each rewritten in python tho to help me learn better.
This uses .NET 8. Change the TargetFramework element to any other version you need, such as net6.0.

```
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net8.0</TargetFramework>
  </PropertyGroup>
</Project>
```

Then in terminal, build and run it:
```
dotnet build
dotnet run
```
